import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const AUILozenge = ({ type, subtle, children }) => {
    const classes = classnames('aui-lozenge', `aui-lozenge-${type}`, {
        'aui-lozenge-subtle': subtle
    });

    return (
        <span className={classes}>{children}</span>
    );
};

AUILozenge.propTypes = {
    type: PropTypes.oneOf(['success', 'error', 'current', 'new', 'moved']),
    subtle: PropTypes.bool,
    children: PropTypes.string.isRequired
};

AUILozenge.defaultProps = {
    type: null,
    subtle: false
};

export default AUILozenge;
