import React from 'react';
import PropTypes from 'prop-types';

const AUIPageHeaderImage = ({ children }) => (
    <div className="aui-page-header-image">
        <span className="aui-avatar aui-avatar-large aui-avatar-project">
            <span className="aui-avatar-inner">{children}</span>
        </span>
    </div>
);

AUIPageHeaderImage.propTypes = {
    children: PropTypes.node
};

export default AUIPageHeaderImage;
