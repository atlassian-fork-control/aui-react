# 1.2.0 (15th Aug 2018)
 - Added new `<ProgressIndicator>` component

# 1.1.0 (26th Feb 2018)
 - Add new [`<Tabs>`](#tabs) and [`<Tab>`](#tab) components 

# 1.0.0 (11th Jan 2018)
 - First stable release
