import React from 'react';
import { shallow, mount } from 'enzyme';
import chai, { expect } from 'chai';
import chaiEnzyme from 'chai-enzyme';
import proxyquire from 'proxyquire';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';

import AUIBlanket from '../src/AUIBlanket';

chai.use(chaiEnzyme());
chai.use(sinonChai);

describe('AUIDialog', () => {
    let AUIDialog;

    beforeEach(() => {
        AUIDialog = proxyquire('../src/AUIDialog', {
            './facades/document': {
                body: () => ({
                    style: { }
                })
            },
            './facades/window': {}
        }).default;
    });

    it('Should render correct mark up', () => {
        const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close'>Test</AUIDialog>);
        expect(wrapper).to.have.descendants('section');
        expect(wrapper).to.have.descendants(AUIBlanket);

        expect(wrapper.find('section')).to.have.prop('role', 'dialog');
        expect(wrapper.find('section')).to.have.className('aui-layer');
        expect(wrapper.find('section')).to.have.className('aui-dialog2');
    });

    it('AUIBlanket should have onClick attribute as null when type=modal', () => {
        const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' type="modal">Test</AUIDialog>);
        expect(wrapper).to.have.descendants(AUIBlanket);
        expect(wrapper.find('AUIBlanket')).to.have.prop('onClick', null);
    });

    it('AUIBlanket should have onClick attribute when type is not modal', () => {
        const fn = () => {};
        const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' onClose={fn}>Test</AUIDialog>);
        expect(wrapper).to.have.descendants(AUIBlanket);
        expect(wrapper.find('AUIBlanket')).to.have.prop('onClick', fn);
    });

    describe('Dialog Size', () => {
        it('Should specify the small size of the dialog according to size attribute.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.have.className('aui-dialog2-small');
        });

        it('Should specify the medium size of the dialog according to size attribute.', () => {
            const wrapper = shallow(<AUIDialog size='medium' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.have.className('aui-dialog2-medium');
        });

        it('Should specify the large size of the dialog according to size attribute.', () => {
            const wrapper = shallow(<AUIDialog size='large' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.have.className('aui-dialog2-large');
        });

        it('Should specify the xlarge size of the dialog according to size attribute.', () => {
            const wrapper = shallow(<AUIDialog size='xlarge' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.have.className('aui-dialog2-xlarge');
        });
    });

    describe('Dialog Type', () => {
        it('Should render a warning dialog as specified.', () => {
            const wrapper = shallow(<AUIDialog size='small' type='warning' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.have.className('aui-dialog2-warning');
            expect(wrapper.find('header')).to.have.descendants('a');
        });

        it('Should render a modal as specified.', () => {
            const wrapper = shallow(<AUIDialog size='small' type='modal' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.not.have.className('aui-dialog2-warning');
            expect(wrapper.find('header')).to.not.have.descendants('a');
        });
    });

    describe('Dialog Header', () => {
        it('Should render a header.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.have.descendants('header');

            expect(wrapper.find('header')).to.have.className('aui-dialog2-header');
        });

        it('Should render a close button in the header.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close'/>);

            expect(wrapper.find('header')).to.have.descendants('a');
            expect(wrapper.find('a')).to.have.className('aui-dialog2-header-close');
            expect(wrapper.find('a')).to.have.descendants('span');
            expect(wrapper.find('span')).to.have.className('aui-iconfont-close-dialog');
            expect(wrapper.find('span')).to.have.className('aui-icon');
            expect(wrapper.find('span')).to.have.className('aui-icon-small');
        });

        it('Should render correct text for close button in the header.', () => {
            let wrapper = shallow(<AUIDialog size='small' closeButtonText='Close'/>);
            expect(wrapper.find('span')).to.have.text('Close');

            wrapper = shallow(<AUIDialog size='small' closeButtonText='Quit'/>);
            expect(wrapper.find('span')).to.have.text('Quit');
        });

        it('Should render main header in the header as specified.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' titleContent='Test a Dialog'/>);
            expect(wrapper.find('header')).to.have.descendants('h2');
            expect(wrapper.find('h2')).to.have.className('aui-dialog2-header-main');
            expect(wrapper.find('h2')).to.have.text('Test a Dialog');
        });

        it('Should render secondary header text in the header as specified.', () => {
            const headerSecondaryContent = <form className='aui'></form>;
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' headerSecondaryContent={headerSecondaryContent}/>);

            expect(wrapper.find('header')).to.have.descendants('div');
            expect(wrapper.find('header').find('div')).to.have.className('aui-dialog2-header-secondary');

            expect(wrapper.find('header').find('div')).to.have.descendants('form');
            expect(wrapper.find('form')).to.have.className('aui');
        });

        it('Should render buttons in the header as specified.', () => {
            const headerActionContent = [<button key='button0' className='aui-button'>Click Me</button>];
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' headerActionContent={headerActionContent}/>);
            expect(wrapper.find('header').find('div.aui-dialog2-header-actions')).to.have.descendants('button');
            expect(wrapper.find('button')).to.have.text('Click Me');
        });
    });

    describe('Dialog Main Content', () => {
        it('Should render a correct body content.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close'><p>Hello World</p></AUIDialog>);
            expect(wrapper.find('section')).to.have.descendants('header');

            expect(wrapper.find('div.aui-dialog2-content')).to.have.descendants('p');
            expect(wrapper.find('div.aui-dialog2-content').find('p')).to.have.text('Hello World');
        });
    });

    describe('Dialog Footer', () => {
        it('Should render a footer.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close'/>);
            expect(wrapper.find('section')).to.have.descendants('footer');

            expect(wrapper.find('footer')).to.have.className('aui-dialog2-footer');
        });

        it('Should render a hint in the footer as specified.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' footerHintContent='This is not a hint'/>);
            expect(wrapper.find('footer').find('div.aui-dialog2-footer-hint')).to.have.text('This is not a hint');
        });

        it('Should render buttons in the footer as specified.', () => {
            const footerActionContent = [<button key='button0' className='aui-button'>Next</button>];
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' footerActionContent={footerActionContent}/>);
            expect(wrapper.find('footer').find('div.aui-dialog2-footer-actions')).to.have.descendants('button');
            expect(wrapper.find('button')).to.have.text('Next');
        });
    });

    describe('Dialog Style', () => {
        it('Should render a style class for root of component.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' styleClass='rootStyle'/>);
            expect(wrapper.find('section')).to.have.className('rootStyle');
        });

        it('Should render a style class for the header.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' headerStyleClass='headerStyle'/>);
            expect(wrapper.find('header')).to.have.className('headerStyle');
        });

        it('Should render a style class for the body container.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' contentStyleClass='bodyStyle'/>);
            expect(wrapper.find('div.aui-dialog2-content')).to.have.className('bodyStyle');
        });

        it('Should render a style class for the footer.', () => {
            const wrapper = shallow(<AUIDialog size='small' closeButtonText='Close' footerStyleClass='footerStyle'/>);
            expect(wrapper.find('footer')).to.have.className('footerStyle');
        });
    });

    describe('Component lifecycle', () => {
        let body;
        let AUIDialog;
        let wrapper;
        let instance;
        let sandbox;
        let windowStub;

        beforeEach(() => {
            sandbox = sinon.sandbox.create();
            windowStub = {
                addEventListener: sandbox.spy(),
                removeEventListener: sandbox.spy(),
            };

            body = {
                style: {
                    overflow: ''
                }
            };

            AUIDialog = proxyquire('../src/AUIDialog', {
                './facades/document': {
                    body: () => body
                },
                './facades/window': windowStub
            }).default;

            wrapper = mount(<AUIDialog size='small' closeButtonText='Close'/>);
            instance = wrapper.instance();
        });

        it('#componentDidMount', () => {
            instance.componentDidMount();

            expect(body.style.overflow).to.equal('hidden');
            expect(windowStub.addEventListener).to.have.been.calledWith('keydown', instance._onKeyDownEsc);

        });

        it('#componentWillUnmount', () => {
            instance.componentWillUnmount();

            expect(windowStub.removeEventListener).to.have.been.calledWith('keydown', instance._onKeyDownEsc);
        });
    });
});
